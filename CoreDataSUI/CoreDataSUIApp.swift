//
//  CoreDataSUIApp.swift
//  CoreDataSUI
//
//  Created by Виталий Вишняков on 06.12.2022.
//

import SwiftUI

@main
struct CoreDataSUIApp: App {
    
    @StateObject private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView().environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
