//
//  EditFoodView.swift
//  CoreDataSUI
//
//  Created by Виталий Вишняков on 06.12.2022.
//

import SwiftUI

struct EditFoodView: View {
    
    var food: FetchedResults<Food>.Element
    
    @Environment(\.managedObjectContext) private var managedObjContext
    @Environment(\.dismiss) private var dismiss
    
    @State private var name = ""
    @State private var calories: Double = .zero
    
    var body: some View {
        Form {
            Section() {
                TextField("\(food.name!)", text: $name)
                    .onAppear {
                        name = food.name!
                        calories = food.calories
                    }
                VStack {
                    Text("Calories: \(Int(calories))")
                    Slider(value: $calories, in: 0...2000, step: 10)
                }
                .padding()
                HStack {
                    Spacer()
                    Button("Submit") {
                        DataController().editFood(food: food, name: name, calories: calories, context: managedObjContext)
                        dismiss()
                    }
                    Spacer()
                }
            }
        }
    }
}
