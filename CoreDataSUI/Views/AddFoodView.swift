//
//  AddFoodView.swift
//  CoreDataSUI
//
//  Created by Виталий Вишняков on 06.12.2022.
//

import SwiftUI

struct AddFoodView: View {
    
    @Environment(\.managedObjectContext) private var managedObjContext
    @Environment(\.dismiss) private var dismiss
    
    @State private var name = ""
    @State private var calories: Double = .zero
    
    var body: some View {
        Form {
            Section() {
                TextField("Food name", text: $name)
                VStack {
                    Text("Calories: \(Int(calories))")
                    Slider(value: $calories, in: 0...1000, step: 10)
                }
                .padding()
                HStack {
                    Spacer()
                    Button("Submit") {
                        DataController().addFood(
                            name: name,
                            calories: calories,
                            context: managedObjContext)
                        dismiss()
                    }
                    Spacer()
                }
            }
        }
    }
}

struct AddFoodView_Previews: PreviewProvider {
    
    static var previews: some View {
        AddFoodView()
    }
}
